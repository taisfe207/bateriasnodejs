const express = require('express');
const app = express();

// Rota para a página inicial
app.get('/', (req, res) => {
  res.send(`
    <html>
      <head>
        <title>Automação</title>
      </head>
      <body>
        <h1>Automação</h1>
        <p>A automação é o processo de realizar tarefas de forma automática, sem a necessidade de intervenção humana. Ela pode ser aplicada em diversas áreas, como na indústria, na agricultura e até mesmo em tarefas domésticas. A automação permite aumentar a eficiência, reduzir erros e liberar os humanos para atividades mais complexas e criativas.</p>
      </body>
    </html>
  `);
});

// Inicia o servidor na porta 3000
app.listen(3000, () => {
  console.log('O servidor está rodando na porta 3000!');
});
